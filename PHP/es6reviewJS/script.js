console.log('hi');


let student = "Mabz";
student = "Ser";
const age = 18;
// age = 13;

// var is like let

for(let i=0; i<5; i++){
  console.log(i)	
}
console.log(i); // i is not define by let

// var has scoping issue
for(var i=0; i<5; i++){
  console.log(i)	
}

console.log(i); // i is accessible due to var

function sayMyName(name){
	alert("Hi " + name);
}
sayMyName("Brandon");

// es6 format
const sayMyNam = name => {
	alert("Hi " + name);
}
sayMyNam("ricardo");


const ages = [21, 22, 23, 24,25]
ages.forEach(function(age){
	console.log(age);
})

// in es6 format
const adults = [18, 19, 20, 21,22]
adults.forEach(adult=>{
	console.log(adult);
})

