// https://jsonplaceholder.typicode.com/users
// fetch api
// with out fetch() is asynchronous
// to make it synchchronous add .then() 
// fetch is required with two .then()
// first fetch().then(transforms the data into a readable format) transforms the data into a readable format
// .then(what we will do with the date  we received) 

let users = [];
fetch("https://jsonplaceholder.typicode.com/users").then(response_from_fetch=>{
  return response_from_fetch.json();
}).then(data_from_response=>{
  // console.log(data_from_response);
  users = data_from_response;
})

fetch("https://api.nasa.gov/planetary/apod?api_key=BhD0CqzOEDdN1ETMCbAKMZB86Kub6m9ja4uMJOUE").then(res=>res.json)// console.log(res); 

.then(res=>{document.getElementById('nasaPhoto').src = res.url;document.getElementById('nasaPhoto').nextElementSibling.innerText = res.title;
  // document.getElementById('nasaPhoto').nextElementSibling.nextElementSibling.innerText = res.date;
  // document.getElementById('nasaPhoto').nextElementSibling.nextElementSiblingnextElementSibling.innerText = res.explanation;

})
