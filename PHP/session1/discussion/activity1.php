<html>
<head>
	<meta charset="UTF-8">
	<title>Activity</title>
</head>
<body>
	<h1>Hello from activity</h1>
	<?php
		$months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		$seasons = ["Winter", "Spring", "Summer", "Fall"];

		foreach($months as $month){

			switch($month){

				case "January":
				echo "The month is $month and the season is $seasons[0] <br>";
				break;

				case "February":
				echo "The month is $month and the season is $seasons[0] <br>";
				break;

				case "March":
				echo "The month is $month and the season is $seasons[0] <br>";
				break;

			}

			if($month === "April" || $month === "May" || $month ==="June"){
				echo "The month is $month and the season is $seasons[1] <br>";
			}else if($month ==="July" || $month === "August" || $month ==="September"){
				echo "The month is $month and the season is $seasons[2] <br>";
			}else if($month ==="October" || $month === "November" || $month ==="December"){
				echo "The month is $month and the season is $seasons[3] <br>";
			}
		}

	?>
</body>
</html>