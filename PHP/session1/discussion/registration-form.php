<!DOCTYPE html>
<html>
<head>
        <title>Registration Form</title>
        <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/darkly/bootstrap.css">
</head>
<body class="bg-primary">
        <h1 class="text-center text-uppercase p-5">REGISTRATION FORM</h1>
        <div class="col-lg-4 offset-lg-4">
                <form action="controllers/registration-process.php" method="POST" class="bg-light p-4">
                        <div class="form-group">
                                <label for="firstName">First Name</label>
                                <input type="text" name="firstName" class="form-control">
                        </div>
                        <div class="form-group">
                                <label for="LastName">Last Name</label>
                                <input type="text" name="lastName" class="form-control">
                        </div>
                        <div class="text-center">
                                <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                        <?php
                        	session_start();
                        	// session_destroy();
                        	if(isset($_SESSION['errorMsg'])){
                        ?>

                        <p>
                        	<?php
                        		echo $_SESSION['errorMsg'];

                        	?>
                        </p>
                        
                        <?php 
                        	}
                        ?>
                       

                </form>
        </div>

</body>
</html>