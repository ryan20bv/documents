<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Intro to PHP</title>
</head>
<body>
	<h1>Hello, guest</h1>
	<p>I'm Brandon, welcome to my page</p>

	<!-- start of PHP coding -->
	<!-- <?php ?>  this is php delimiter -->

	<?php 
		// echo 'Hello <br> World'
	?>	

	<!-- variables -->
	<!-- to define the length of a variable -->
	<?php 
		
		// echo " <br>";

	?>	


	<?php 
		// $student = "kaka"; 	//string
		// $age = 27;  //integer
		// $pi = 3.14; //float
		// $single = true; //bolean
		// echo "Hi $student! I'm $age years old";
		// echo "<br>";
		// echo "the value of is $pi and $single <br>";
	?>


	<!-- operators -->

	<?php 
		// $num1 = 20;
		// $num2 = 5;
		// $sum = $num1 + $num2;
		// $substraction = $num1 - $num2;
		// $product = $num1 * $num2;
		// $modulo = $num1%$num2;
		// echo "$sum, $substraction, $product <br>";
		
		// echo "$modulo";
		// ++$modulo;
		// echo "<br>";
		// echo "$modulo";
		// // echo ++"$modulo";
		// // echo "$modulo";
		// echo "<br>";

	?>


	<!-- control structure -->

	<?php 
		// $num1 = 20;
		// $num2 = 17;

		// if($num1 > $num2){
		// 	echo "$num1 is greater than $num2 <br>";
		// }


	?>




	<?php 
		// $today = "Thursday";
		// switch($today){
		// 	case "Monday":
		// 	echo "I love Mondays";
		// 	break;
		// 	case "Thursday":
		// 	echo "One more day";
		// 	break;
		// 	case "Sunday":
		// 	echo "Pray for Taal";
		// 	break;
		// 	default:
		// 	echo "May pasok ba?";
		// 	break;

		// }

	?>

	<!-- Arrays -->


	<?php 
		$students = ["Pochie","Ona", "Archie"];
		$grades = ["Math"=>98, "English"=>96, "Chemistry"=>91];   //key value pair //associative array 
		// var_dump($student);
		
		

		// to display individually;
		// echo "Hello $student[0]";

		// to display individually in sequence

		// foreach(nameOfArray as indivVariable){
		// 	// code here
		// foreach($students as $student){
		// 	echo "Hello $student <br>";

		// }
		// var_dump($grades["Chemistry"]);
		// die()
		foreach($grades as $key=>$grade){
			echo "My $key grade is  $grade <br>";
		}

	?>

		<h1>I'm another Heading</h1>


</body>
</html>