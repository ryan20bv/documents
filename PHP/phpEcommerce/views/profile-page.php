<?php
    require "../partials/template.php";

    function get_title(){
        echo "Profile Page";
    }
    function get_body_contents(){
        require "../controllers/connection.php";
        

        $firstName = $_SESSION['user']['firstName'];
        $lastName = $_SESSION['user']['lastName'];
        $userId = $_SESSION['user']['id'];
    
 ?>
    <h1 class="text-center py-3">Profile Page</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h1 class = "text-center py-3"><?php echo $firstName;echo " "; echo $lastName  ?></h1>
                <p class="text-center py-3"><?php echo $_SESSION['user']['email']?></p>
            </div>
            <div class="col-lg-4">
                <h3>Address:</h3>
                <ul>
                    <?php 
                        $address_query = "SELECT * FROM addresses WHERE user_id = $userId ";
                        $addresses = mysqli_query($conn, $address_query);
                        foreach ($addresses as $indiv_address) {
                    ?>
                        <li><?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city'] . ", " . $indiv_address['zipCode'] ?>
                        </li>
                    <?php
                        }

                     ?>
                </ul>
                <form action="../controllers/add-address-process.php" method="POST">
                    <div class="form-group">
                        <label for="address1">Address 1:           
                        </label>
                        <input type="text" name="address1" class="form-control">
                   </div>
                    <div class="form-group">
                        <label for="address2">Address 2:           
                        </label>
                        <input type="text" name="address2" class="form-control">
                    </div>
                     <div class="form-group">
                        <label for="city">City:           
                        </label>
                        <input type="text" name="city" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="zipCode">zipCode:           
                        </label>
                        <input type="text" name="zipCode" class="form-control">
                    </div>
                    <input type="hidden" name="user_id" value="<?php echo $userId?>">
                    <button class="btn btn-secondary" type="submit">Add Address</button>
                    
                </form>


               

               <!--  <img class="border" height= "100px" src="" alt="">
                <form action="">
                    <div class="form-group">
                        <input type="file" name="image" class="form-control">
                        <button submit="submit" class="btn btn-info">upload</button>
                    </div>
                </form> -->
            </div>
            <div class="col-lg-4">
                <h3>Contact:</h3>
                <ul>
                    <?php 
                        $contact_query = "SELECT * FROM contacts WHERE user_id = $userId ";
                        $contacts = mysqli_query($conn, $contact_query);
                        foreach ($contacts as $indiv_contact) {
                    ?>
                        <li><?php echo $indiv_contact['contactNo']?>
                        </li>
                    <?php
                        }

                     ?>
                </ul>

                 <form action="../controllers/add-address-process.php" method="POST">
                    <div class="form-group">
                        <label for="contact">Contact :           
                        </label>
                        <input type="number" name="contact" class="form-control">
                   </div>
                    <input type="hidden" name="user_id" value="<?php echo $userId?>">
                    <button class="btn btn-secondary" type="submit">Add Contact</button>
                    
                </form>
            </div>
        </div>
        <div>
            <h1 class="text-center py-5">History Page</h1>
            <hr class="border-white">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Items</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $userId = $_SESSION['user']['id'];
                    $order_query = "SELECT * FROM orders WHERE user_id = $userId";
                    $orders = mysqli_query($conn, $order_query);

                    foreach($orders as $indiv_order){

                ?>

                <tr>
                    <td><?php echo $indiv_order['id']?></td>
                    <td>
                        <?php
                        $order_Id = $indiv_order['id']; 
                        $items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = order_Id";
                        $items = mysqli_query($conn, $items_query);

                        foreach($items as $indiv_item){
                        ?>

                         <span><?php echo $indiv_item['name']?></span><br>

                        <?php   
                        }

                        ?>
                    </td>
                    <td><?php echo $indiv_order['total']?></td>
                </tr>

                <?php
                }

                ?>

                </tbody>
            </table>
      
        </div>   
    </div> 
<!-- <?php       
    }
?> -->