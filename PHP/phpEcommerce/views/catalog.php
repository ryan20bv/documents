<?php 
	require "../partials/template.php";
	

	function get_title(){
		echo "Catalog";
	}
	// // require connection
	// require "../controllers/connection.php";

	function get_body_contents(){
		// require connection
	require "../controllers/connection.php";

?>
	
	<h1 class="text-center py-5">THIS - 3S</h1>

	<!-- Item List -->
	<div class="row">
			<?php 
				$items_query ="SELECT * FROM items";
				$items = mysqli_query($conn, $items_query);
				// var_dump($items);
				// die();
				foreach ($items as $indiv_item) {
					// var_dump($indiv_item);
					// // die();
			?>
				<div class="col-lg-4 py-2">
					<div class="card">
						<img class="card-img-top" height="200px"  src="<?php echo $indiv_item['imgPath'] ?>" alt=""	>
						<div class="card body">
							<h4 class="card-title"><?= $indiv_item['name']?></h4>  
							<p class="card-text">Price: Php <?= $indiv_item['price']?></p>
							<p class="card-text">Description: <?= $indiv_item['description']?></p>
							<p class="card-text">Category: 
								<?php  
									$catId = $indiv_item['category_id'];
									$category_query = "SELECT * FROM categories WHERE id = $catId";
									// for one row only mysqli_fetch_assoc
									$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
									// to publish
									// if($category['id']=$catId){
									// var_dump($category);

									// }
									echo $category['name'];
									
								?>
							</p>  
						</div>
						<?php if(isset($_SESSION['user']) && $_SESSION['user']['email']=="admin@admin.admin" && $_SESSION['user']['role_id']==1){

						?>
						<div class="card-footer">
							<a href="../controllers/delete-item-process.php?id=<?php echo $indiv_item['id']?>"
						        class="btn btn-danger">Delete Item</a>
						    <a href="edit-item.php?id=<?php echo $indiv_item['id']?>"class="btn btn-success">Edit Item</a>
						</div>
						<?php
						}
						?>
						<div class="car-footer">
							<!-- comment out because of Javascript to prevent refreshing of page from the top -->
							<!-- <form action="../controllers/add-to-cart-process.php" method="POST">  -->
								<!-- <input type="hidden" name="id" value=""> echo has been removed-->
								<input type="number" name="cart" class="form-control" value="1">
								<!-- data-id for html which is dynamic while id is for single only -->
								<button type="button" class="addToCart btn btn-primary" data-id="<?php echo $indiv_item['id']?>">Add to Cart</button>
							<!-- </form> -->

							
							
						</div>
						
					</div>
					
				</div>

			<?php		
				}
			 ?>
		
	</div>
	<script src="../assets/scripts/add-to-cart.js" type="text/javascript"></script>

<?php 

	}
 ?>