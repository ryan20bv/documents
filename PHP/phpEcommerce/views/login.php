<?php 
	require "../partials/template.php";
	function get_title(){
		echo "Log-In Page";
	}
	function get_body_contents(){

?>
	<h1 class="text-center py-3">Log-In Page</h1>
	<div class="col-lg-6 offset-lg-3">
		<form action="../controllers/login-process.php" method="POST">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" class="form-control" id="email" ><span class="validation"></span>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control" id="password"><span class="validation"></span>
					</div>
					<div class="text-center">
						<button id="loginUser" type="button" class="btn btn-info">Login</button>
						
					</div>
					<p>Not yet Registered? <a href="login.php">Register</a></p>
					
		</form>
				
	</div>
			
	<script src="../assets/scripts/login.js" type="text/javascript"></script>


<?php
	}
 ?>