// /javascript is child of ecmascript

// steps we will take
// 1. we need to capture all the add to cart button
// 2. we need to attach an event listener to each of the buttons
// 3. we need to get the data (the id) from the button
// 4. we need to get the quantity from the input
// 5. we need to check if the quantity  > 0
// 6. if yes, send the data to the controller via... fetch



// 1. we need to capture all the add to cart button
	// querySelectorAll is array like
let addToCartBtns = document.querySelectorAll(".addToCart");
// console.log(addToCartBtns);

// 2. we need to attach an event listener to each of the buttons

addToCartBtns.forEach(indiv_btn=>{
	indiv_btn.addEventListener('click', btn=>{
		let id = btn.target.getAttribute("data-id")
		// console.log(id)
		// 3. we need to get the data (the id) from the button
		let quantity = btn.target.previousElementSibling.value;
		// console.log(quantity);
		// 4. we need to get the quantity from the input
		// 5. we need to check if the quantity  > 0
		if(quantity <=0){
			alert("Please enter valid quantity");
		}else{
			// send to controller back end
			// for PHP to JS to talk / access each other
			// to create a data to be used in add-to-cart-process from JS to PHP
			//use new FormData which is clas of Java script
			let data = new FormData;
			data.append("id", id);
			data.append("cart",quantity);

			fetch("../../controllers/add-to-cart-process.php", {
				method: "POST", body: data 
			})
			.then(response=>{
				// console.log(response) should be status 200
				return response.text();
			})
			.then(res=>{
				document.getElementById('cartCount').textContent = res;
			})
		}
	})
})


