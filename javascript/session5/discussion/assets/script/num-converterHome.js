// console.log("tsamba")


// convert numbers to words

// for single
let singleDigit =  ["zero", "one", "two", "three","four", "five", "six", "seven", "eight", "nine" ]
let teens = [" ", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]

let tens = [" ", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"]



// for single Digit (1-10)

function numConverter(inputNo){
    if (inputNo >=0 && inputNo <1000){
        // for single digit (0-9)
        if (inputNo < 10 && inputNo >= 0){
            return singleDigit[inputNo];  
        }
        // for (10 - 19)

        if (inputNo>10 && inputNo < 20){
            return teens[inputNo-10];
        }
        // for number divisible by 10 (10 - 100)
        if (inputNo%10 ===0 && inputNo<100){
            let tenValue = (inputNo/10)
                return tens[tenValue]
        }
        // for other numbers (20 - 99)
        if (inputNo>=20 && inputNo<100){
            let singleValue = (inputNo%10)
            let tenValue = ((inputNo-singleValue)/10)
                return tens[tenValue] + " " + singleDigit[singleValue]
        }
        // for number more than 100 < 110
        if (inputNo > 100 && inputNo < 110){
            let singleValue = (inputNo%100)
            let hundredValue = ((inputNo-singleValue)/100)
            return singleDigit[hundredValue] + " hundred " + singleDigit[singleValue]
        }
        // for divisible by 100 (100 - 999)
        if (inputNo%100 ===0 && inputNo < 999 ){
            let hundredValue = (inputNo/100)
                return singleDigit[hundredValue] + " hundred "
        }
        // for hundred and teens (111-120)
        if (inputNo >= 111 && inputNo <120){
            let singleValue = (inputNo%10)
            let hundredValue = (inputNo-(singleValue+10))/100
                return singleDigit[hundredValue] + " hundred " + teens[singleValue]
        } 
        //  for numbers divisivle by 10 ( 100 to 999)
        if (inputNo >= 100 && inputNo < 1000 && inputNo%10 ===0 ){
            let tenValue = ((inputNo/10)%10)
            let hundredValue = (inputNo-(tenValue*10))/100
                return singleDigit[hundredValue] + " hundred " + tens[tenValue]
        }
        //  for other number (100 - 999 ) with ones
        if (inputNo>120 && inputNo%10 !==0){
            let singleValue = (inputNo%10)
            let tenValue = (((inputNo - singleValue)/10)%10)
            let hundredValue = ((inputNo-((tenValue*10) + singleValue))/100)
                return singleDigit[hundredValue] + " hundred " + tens[tenValue] + " " + singleDigit[singleValue] 
        }
    }
    // for numbers (1000 - 9999)
    if (inputNo >= 1000 && inputNo < 10000){
        // for numbers ending in (1-9) with hundred  and tens  (sample 1891,9999)
        if (inputNo%1000 > 110){
            //with teens
            if (inputNo%100<20 && inputNo%100 > 10){
                let teenValue = ((inputNo%100)%10)
                let hundredValue =  (((inputNo -(teenValue+10))/100)%10)
                let thousandValue = ((inputNo - ((hundredValue*100)+(teenValue+10)))/1000)
                    return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred " + teens[teenValue]
            }else 
            //more than 20 last two digit
            if (inputNo%100>20){
                let singleValue = (inputNo%10)
                let tenValue = (((inputNo-singleValue)/10)%10)
                let hundredValue =  (((inputNo -((tenValue*10)+singleValue))/100)%10)
                let thousandValue = ((inputNo - ((hundredValue*100)+(tenValue*10)+singleValue))/1000)
                    return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred " + tens[tenValue] + " " + singleDigit[singleValue]
            }
            

        }else 
        // for numbers ending in 0 with hundred and tens
        // with 0 ones
        if (inputNo%100 < 10){
            let tenValue = ((inputNo/10)%10)
            let hundredValue = (((inputNo-(tenValue*10))/100)%10)
            let thousandValue = ((inputNo-((hundredValue*100)+(tenValue*10)))/1000)
                return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred " + tens[tenValue]
        }



       
        // for single digit (1000-1009)
        if (inputNo < 1010 && inputNo >= 1001){
            let singleValue = (inputNo%1000)
            let thousandValue = ((inputNo - singleValue)/1000)
            return singleDigit[thousandValue] + " thousand " + singleDigit[singleValue];  
        }
        // for numbers ending in 11-19 (1000-9999)
        if (inputNo>1010 && inputNo < 1020){
            let teenValue = (inputNo%10)
            let thousandValue = ((inputNo - (teenValue+10))/1000)
            return singleDigit[thousandValue] + " thousand " + teens[teenValue];
        }
        // for number divisible by 1000 (1000 - 9999)
        if (inputNo%1000 === 0 && inputNo >= 1000 && inputNo<10000 ){
            let thousandValue = (inputNo/1000)
                return singleDigit[thousandValue] + " thousand "
        }
        // for number divisible by 10 (1110 - 9999)
         
        // for divisible by 100 (1100 - 9999)
         if (inputNo >= 1100 && inputNo < 9999 && inputNo%1000 !==0 && inputNo%100 === 0){
            let hundredValue = ((inputNo/100)%10)
            let thousandValue = ((inputNo - (hundredValue*100))/1000)
                return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred"
        }
         //  for numbers without 0 ( 1121 to 9999)
         if (inputNo >= 1121 && inputNo < 10000 && inputNo%10 !==0 ){
            let singleValue = (inputNo%10)
            let tenValue = (((inputNo-singleValue)/10)%10)
            let hundredValue = (((inputNo-((tenValue*10)+singleValue))/100)%10)
            let thousandValue = ((inputNo-((hundredValue*100)+(tenValue*10)+singleValue))/1000)
                return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred " + tens[tenValue] + " " + singleDigit[singleValue]
        } 
        
        
        //  for numbers without tens but with ones (1101 -9999 ) with ones
        if (inputNo>=1101 && inputNo < 10000 && inputNo%10 !==0){
            let singleValue = (inputNo%10)
            let hundredValue = (((inputNo-singleValue)/100)%10)
            let thousandValue = ((inputNo-((hundredValue*100)+singleValue))/1000)
                return singleDigit[thousandValue] + " thousand " + singleDigit[hundredValue] + " hundred " + singleDigit[singleValue]
    
        }
       
        // for other numbers  with tens and ones (1020 - 9999)
        if (inputNo>1020 && inputNo<10000 && inputNo%10 !==0){
            let singleValue = (inputNo%10)
            let tenValue = (((inputNo-singleValue)/10)%10)
            let thousandValue = ((inputNo - ((tenValue*10)+singleValue))/1000)
                return singleDigit[thousandValue] + " thousand " + tens[tenValue] + " " + singleDigit[singleValue]
        }
        
     
        //  for numbers divisivle by 10 ( 1010 to 9999)
        if (inputNo >= 1010 && inputNo < 10000 && inputNo%100 !==0 ){
            let tenValue = ((inputNo/10)%10)
            let thousandValue = (inputNo-(tenValue*10))/1000
                return singleDigit[thousandValue] + " thousand " + tens[tenValue]
        }
                                
       
    }
      

}

