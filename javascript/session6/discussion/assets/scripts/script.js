// console.log("hello from B55")
// document.write("hello from B55")

// Objects
// name: "Brandon"

let student = {
	name: "Brandon",
	age: 21,
	address: {
		stName: "Oxford St.",
		barangay: "Brgy. 121",
		city: "Makati City",
		},
	favoriteFood: ["Sinigang", "Tinola", "Fried Chicken"],
	mother: {
		name : "Eloisa",
		age : 62,
		address : {
			stName: "Oxford St.",
			barangay : "Brgy. 121",
			city : "Makati City",
			},
		}
}


student.favoriteFood.forEach(function(food){
	console.log(food);
})

// to add
student.address
student["address"];
student.studentID = "AC1543";
student['year'] = "4th Year";
student.year = 4;
// to add an object
student.courseDetails = {college: "Science",
		department: "Chemistry",
		course: "BS Chemistry",
	}

// to delete
delete student.age



